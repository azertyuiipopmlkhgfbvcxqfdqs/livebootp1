
citbx_use "dockerimg"

target_build_exec_hook() {
    local hook_function=$1
    shift
    if [ -f "targets/$LIBEBOOTP_TARGET_IMAGE_NAME/build-hooks.sh" ]; then
        cd targets/$LIBEBOOTP_TARGET_IMAGE_NAME
        source "build-hooks.sh"
        if [[ "$(type -t $hook_function)" == "function" ]]; then
            $hook_function "$@"
        else
            print_note "HOOK[$hook_function]: Function $hook_function not found, skipping this hook"
        fi
    else
        print_note "HOOK[$hook_function]: File targets/$LIBEBOOTP_TARGET_IMAGE_NAME/build-hooks.sh not found, skipping this hook"
        return
    fi
    cd "$CI_PROJECT_DIR"
}

job_setup() {
    # Only on workstation, set the suitable tag prefix if not set
    local job_prefix=${CI_JOB_NAME%-build}
    CI_COMMIT_TAG="$job_prefix/${CI_COMMIT_TAG#$job_prefix/}"
}

job_main() {
    local pattern='^target-.*-build$'
    if ! [[ $CI_JOB_NAME =~ $pattern ]]; then
        print_critical "Target image job name must begin by 'target-' and terminate by '-build'"
    fi
    LIBEBOOTP_TARGET_IMAGE_NAME=${CI_JOB_NAME%-build}
    pattern='^'"$LIBEBOOTP_TARGET_IMAGE_NAME"'/.*$'
    if ! [[ $CI_COMMIT_TAG =~ $pattern ]]; then
        print_critical "This job cannot be launched with the following tag '$CI_COMMIT_TAG'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^$LIBEBOOTP_TARGET_IMAGE_NAME\/.*\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag $LIBEBOOTP_TARGET_IMAGE_NAME/x.y.z"
    fi
    LIBEBOOTP_TARGET_IMAGE_NAME=${LIBEBOOTP_TARGET_IMAGE_NAME#target-}
    LIBEBOOTP_TARGET_IMAGE_VERSION=${CI_COMMIT_TAG#${CI_JOB_NAME%-build}/}
    LIBEBOOTP_TARGET_IMAGE_TAG="$CI_REGISTRY_IMAGE/$LIBEBOOTP_TARGET_IMAGE_NAME:$LIBEBOOTP_TARGET_IMAGE_VERSION"
    # Build docker image
    if [ "$LIBEBOOTP_TARGET_IMAGE_NAME" != "base" ]; then
        LIBEBOOTP_DOCKER_BUILD_ARGS+=(--build-arg "CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE")
    fi
    target_build_exec_hook target_docker_build_before
    LIBEBOOTP_DOCKER_BUILD_ARGS+=(-t "$LIBEBOOTP_TARGET_IMAGE_TAG")
    docker build "${LIBEBOOTP_DOCKER_BUILD_ARGS[@]}" \
        "targets/$LIBEBOOTP_TARGET_IMAGE_NAME/"
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$LIBEBOOTP_TARGET_IMAGE_TAG"
    fi
}

job_after() {
    local retcode=$1
    target_build_exec_hook target_docker_build_after $1
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$LIBEBOOTP_TARGET_IMAGE_TAG\" successfully generated"
    fi
}
